﻿using BusinessLayer;
using DataLayer.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace IKolokvijum.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class Student1Controller : ControllerBase
    {
        IStudentBussines sbus;

        public Student1Controller(IStudentBussines sbus)
        {
            this.sbus = sbus;
            this.sbus.SetConnectionString(Startup.ConnectionString);
        }

        // GET: api/<Student1Controller>
        [HttpGet]
        public List<Student> Get()
        {
            return sbus.GetAllStudents();
        }

        // GET api/<Student1Controller>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<Student1Controller>
        [HttpPost("insert")]
        public bool Post([FromBody] Student value)
        {
            return sbus.InsertStudenst(value);
        }

        // PUT api/<Student1Controller>/5
        [HttpPut("update")]
        public bool Put([FromBody] Student s)
        {
            return sbus.UpdateStudents(s);
        }

        // DELETE api/<Student1Controller>/5
        [HttpDelete("delete")]
        public bool Delete([FromBody] Student s)
        {
            return sbus.DeleteStudents(s);
        }
    }
}
