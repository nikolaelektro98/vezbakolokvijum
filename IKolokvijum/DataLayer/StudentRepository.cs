﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace DataLayer
{
    public class StudentRepository : IStudentRepository
    {
        public string connectionString;

        public bool DeleteStudents(Student s)
        {
            bool isSucces = false;
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                string sqlcom = "DELETE FROM STUDENTI WHERE BrojIndeksa = @BrojIndeksa ";
                SqlCommand com = new SqlCommand(sqlcom, con);
                
                com.Parameters.AddWithValue("@BrojIndeksa", s.BrojIndeksa);

                con.Open();
                int rows = com.ExecuteNonQuery();

                if (rows > 0)
                {
                    isSucces = true;
                }
                else
                {
                    isSucces = false;
                }
            }
            return isSucces;
        }

        public List<Student> GetAllStudents()
        {
            List<Student> students = new List<Student>();

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();
                string command = "SELECT * FROM STUDENTI";
                SqlCommand com = new SqlCommand(command,con);
                SqlDataReader read = com.ExecuteReader();

                while (read.Read())
                {
                    Student s = new Student();
                    s.Ime = read.GetString(1);
                    s.Prezime = read.GetString(2);
                    s.BrojIndeksa = read.GetString(3);
                    s.ProsecnaOcena = read.GetDecimal(4);
                    students.Add(s);




                }




                con.Close();

            }
            
            return students;
        }

        public bool InsertStudenst(Student s)
        {
            bool isSucces = false;
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                string sqlcom = "INSERT INTO STUDENTI (Ime,Prezime,BrojIndeksa,ProsecnaOcena) VALUES(@Ime,@Prezime,@BrojIndeksa,@ProsecnaOcena)";
                SqlCommand com = new SqlCommand(sqlcom,con);
                com.Parameters.AddWithValue("@Ime", s.Ime);
                com.Parameters.AddWithValue("@Prezime", s.Prezime);
                com.Parameters.AddWithValue("@BrojIndeksa", s.BrojIndeksa);
                com.Parameters.AddWithValue("@ProsecnaOcena", Decimal.Parse(s.ProsecnaOcena.ToString()));

                con.Open();
                int rows = com.ExecuteNonQuery();

                if (rows > 0)
                {
                    isSucces = true;
                }
                else
                {
                    isSucces = false;
                }



            }
            return isSucces;
            }

        public void SetConnectionString(string connectionString)
        {

            this.connectionString = connectionString;
        }

        public bool UpdateStudents(Student s)
        {
            bool isSucces = false;
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                string sqlcom = "UPDATE STUDENTI SET Ime = @Ime, Prezime = @Prezime, ProsecnaOcena= @ProsecnaOcena WHERE BrojIndeksa= @BrojIndeksa";
                SqlCommand com = new SqlCommand(sqlcom, con);
                com.Parameters.AddWithValue("@Ime", s.Ime);
                com.Parameters.AddWithValue("@Prezime", s.Prezime);
                com.Parameters.AddWithValue("@BrojIndeksa", s.BrojIndeksa);
                com.Parameters.AddWithValue("@ProsecnaOcena", s.ProsecnaOcena);

                con.Open();
                int rows = com.ExecuteNonQuery();

                if (rows > 0)
                {
                    isSucces = true;
                }
                else
                {
                    isSucces = false;
                }



            }
            return isSucces;
        }
    }
}
