﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer
{
   public interface IStudentRepository
    {
         void SetConnectionString(string connectionString);

        List<Student> GetAllStudents();
        bool InsertStudenst(Student s);
        bool DeleteStudents(Student s);

        bool UpdateStudents(Student s);


    }
}
