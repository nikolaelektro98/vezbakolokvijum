﻿using DataLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer
{
    public class StudentBussines : IStudentBussines
    {
        private IStudentRepository srep;

        public StudentBussines(IStudentRepository srep)
        {
            this.srep = srep;
        }

        public bool DeleteStudents(Student s)
        {
            return srep.DeleteStudents(s);
        }

        public List<Student> GetAllStudents()
        {
            List<Student> list = srep.GetAllStudents();
            if (list.Count > 0)
            {
                return list;
            }
            else
                return null;
        }

        public bool InsertStudenst(Student s)
        {
            return srep.InsertStudenst(s);
        }

        public void SetConnectionString(string connectionString)
        {
            srep.SetConnectionString(connectionString);
        }

        public bool UpdateStudents(Student s)
        {
            return srep.UpdateStudents(s);
        }
    }
}
